﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(cotizador.Startup))]
namespace cotizador
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

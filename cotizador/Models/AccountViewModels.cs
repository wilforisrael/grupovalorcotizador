﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace cotizador.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Código")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "¿Recordar este explorador?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Correo electrónico")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [Display(Name = "¿Recordar cuenta?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar contraseña")]
        [Compare("Password", ErrorMessage = "La contraseña y la contraseña de confirmación no coinciden.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "El número de caracteres de {0} debe ser al menos {2}.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar contraseña")]
        [Compare("Password", ErrorMessage = "La contraseña y la contraseña de confirmación no coinciden.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class elements
    {
        public string Name { set; get; }
        public Int64 ProductID { set; get; }
        public decimal Price { set; get; }
        public Int64 Existence { set; get; }
        public decimal Discount { set; get; }
        public Int64 PackageID { set; get; }
        public string NamePackage { set; get; }
        public decimal Cant { set; get; }
        public decimal PpDisc { set; get; }
    }
    public class userData
    {
        public string FirstName { set; get; }
        public string Lastname { set; get; }
        public string RUC { set; get; }
        public Int64 VendedorID { set; get; }
        public string VendedorName { set; get; }
        public decimal Total { set; get; }
        public Int64 QuoteStatuID { set; get; }
        public string VendorID { set; get; }
    }

    public class ResultadosViewModel
    {
        public string FirstName { set; get; }
        public string Lastname { set; get; }
        public string RUC { set; get; }
        public string VendedorName { set; get; }
        public decimal Total { set; get; }
        public DateTime Fecha { set; get; }
        public Int64 QuoteID { set; get; }
 
    }
    public class ForArticleViewModel
    {
        [Key]
        public Int64 QuoteID { set; get; }
        public Int64 ProductID { set; get; }
        public string Name { set; get; }
        public decimal Price { set; get; }
        public decimal Cant { set; get; }
        public decimal Discount { set; get; }
        public string NamePackage { set; get; }
    }
    public class ForArticle2ViewModel
    {
        public Int64 QuoteID { set; get; }
        public Int64 ProductID { set; get; }
        public string Name { set; get; }
        public decimal Price { set; get; }
        public decimal Cant { set; get; }
        public decimal Discount { set; get; }
        public string NamePackage { set; get; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }
    }
}

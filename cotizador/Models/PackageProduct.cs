﻿using cotizador.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrupoValorCotizador.Models
{
    public class PackageProduct
    {
        public Int64 PackageProductID { set; get; }
        public Int64 PackageID { set; get; }
        public virtual Package Package { set; get; }
        public Int64 ProductID { set; get; }
        public virtual Product Product { set; get; }
        public decimal Cant { set; get; }
        public decimal Discount { set; get; }
        public decimal Tax { set; get; }
    }
}
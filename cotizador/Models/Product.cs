﻿using cotizador.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrupoValorCotizador.Models
{
    public class Product
    {
        public Int64 ProductID { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public decimal Price { set; get; }
        public Int64 Existence { set; get; }
        public decimal Discount { set; get; }
        public decimal Tax { set; get; }
        public virtual ICollection<QuoteProduct> QuoteProduct { set; get; }
        [Newtonsoft.Json.JsonIgnoreAttribute]
        public virtual ICollection<PackageProduct> PackageProduct { set; get; }
    }
}
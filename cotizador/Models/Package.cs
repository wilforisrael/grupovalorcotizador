﻿using cotizador.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrupoValorCotizador.Models
{
    public class Package
    {
        public Int64 PackageID { set; get; }
        public string NamePackage { set; get; }
        public virtual ICollection<PackageProduct> PackageProduct { set; get; }
        public virtual ICollection<QuoteProduct> QuoteProduct { set; get; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace cotizador.Models
{
    public class PackagesproViewModels
    {
        public string Name { set; get; }
        public Int64 ProductID { set; get; }
        public decimal Price { set; get; }
        public Int64 Existence { set; get; }
        public decimal Discount { set; get; }
        public Int64 PackageID { set; get; }
        public string NamePackage { set; get; }
        public decimal PpCant { set; get; }
        public decimal PpDisc { set; get; }

    }
}
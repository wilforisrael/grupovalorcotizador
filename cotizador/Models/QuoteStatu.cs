﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrupoValorCotizador.Models
{
    public class QuoteStatu
    {
        public Int64 QuoteStatuID { set; get; }
        public string TextQuote { set; get; }
        public virtual ICollection<Quote> Quotes { set; get; }
    }
}
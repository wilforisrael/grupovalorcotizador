﻿using GrupoValorCotizador.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrupoValorCotizador.Models
{
    public class Vendor
    {
        public Int64 VendorID { set; get; }
        public string Vendorname { set; get; }
        public virtual ICollection<Quote> Quotes { set; get; }
    }
}
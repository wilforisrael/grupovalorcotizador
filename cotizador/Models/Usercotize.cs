﻿using GrupoValorCotizador.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrupoValorCotizador.Models
{
    public class Usercotize
    {
        public Int64 UsercotizeID { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string RUC { set; get; }
         public virtual ICollection<Quote> Quotes { set; get; }
    }
}
﻿using cotizador.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GrupoValorCotizador.Models
{
    public class Quote
    {
        public Int64 QuoteID { set; get; }
        public string UsercotizeID { set; get; }
        public virtual Usercotize Usercotize { set; get; }
        public string VendorID { set; get; }
        public virtual Vendor Vendor { set; get; }
        public Int64 QuoteStatuID { set; get; }
        public virtual QuoteStatu QuoteStatu { set; get; }
        public DateTime Date { set; get; }
        public int IVA { set; get; }
        public decimal Discount { set; get; }
        public decimal Total { set; get; }
        public decimal TotalPay { set; get; }
        public decimal Tax { set; get; }
        public virtual ICollection<QuoteProduct> QuoteProduct { set; get; }
    }
}
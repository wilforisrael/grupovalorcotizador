﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using GrupoValorCotizador.Models;

namespace cotizador.Models
{
    // Puede agregar datos del perfil del usuario agregando más propiedades a la clase ApplicationUser. Para más información, visite http://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string RUC { set; get; }
       // public virtual ICollection<Quote> Quotes { set; get; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar aquí notificaciones personalizadas de usuario
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            base.Configuration.ProxyCreationEnabled = false;
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<GrupoValorCotizador.Models.QuoteStatu> QuoteStatus { get; set; }
        public System.Data.Entity.DbSet<GrupoValorCotizador.Models.Quote> Quotes { get; set; }
        public System.Data.Entity.DbSet<GrupoValorCotizador.Models.QuoteProduct> QuoteProducts { get; set; }
        public System.Data.Entity.DbSet<GrupoValorCotizador.Models.Product> Products { get; set; }
        public System.Data.Entity.DbSet<GrupoValorCotizador.Models.PackageProduct> Packages { get; set; }

        public System.Data.Entity.DbSet<GrupoValorCotizador.Models.Package> Packages1 { get; set; }
        public System.Data.Entity.DbSet<GrupoValorCotizador.Models.Vendor> Vendors { get; set; }

        public System.Data.Entity.DbSet<GrupoValorCotizador.Models.Usercotize> Usercotizes { get; set; }

        public System.Data.Entity.DbSet<cotizador.Models.ForArticleViewModel> ForArticleViewModels { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace cotizador.Models
{
    public class CreateQuoteViewModel
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string RUC { set; get; }
        public string search { set; get; }
    }
}
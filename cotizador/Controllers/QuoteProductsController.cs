﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GrupoValorCotizador.Models;
using cotizador.Models;

namespace cotizador.Controllers
{
    public class QuoteProductsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: QuoteProducts
        public ActionResult Index()
        {
            var quoteProducts = db.QuoteProducts.Include(q => q.Product).Include(q => q.Quote);
            return View(quoteProducts.ToList());
        }

        // GET: QuoteProducts/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QuoteProduct quoteProduct = db.QuoteProducts.Find(id);
            if (quoteProduct == null)
            {
                return HttpNotFound();
            }
            return View(quoteProduct);
        }

        // GET: QuoteProducts/Create
        public ActionResult Create()
        {
            ViewBag.ProductID = new SelectList(db.Products, "ProductID", "Name");
            ViewBag.QuoteID = new SelectList(db.Quotes, "QuoteID", "UserID");
            return View();
        }

        // POST: QuoteProducts/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "QuoteProductID,QuoteID,ProductID,Cant,Discount,Tax")] QuoteProduct quoteProduct)
        {
            if (ModelState.IsValid)
            {
                db.QuoteProducts.Add(quoteProduct);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ProductID = new SelectList(db.Products, "ProductID", "Name", quoteProduct.ProductID);
            ViewBag.QuoteID = new SelectList(db.Quotes, "QuoteID", "UserID", quoteProduct.QuoteID);
            return View(quoteProduct);
        }

        // GET: QuoteProducts/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QuoteProduct quoteProduct = db.QuoteProducts.Find(id);
            if (quoteProduct == null)
            {
                return HttpNotFound();
            }
            ViewBag.ProductID = new SelectList(db.Products, "ProductID", "Name", quoteProduct.ProductID);
            ViewBag.QuoteID = new SelectList(db.Quotes, "QuoteID", "UserID", quoteProduct.QuoteID);
            return View(quoteProduct);
        }

        // POST: QuoteProducts/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "QuoteProductID,QuoteID,ProductID,Cant,Discount,Tax")] QuoteProduct quoteProduct)
        {
            if (ModelState.IsValid)
            {
                db.Entry(quoteProduct).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ProductID = new SelectList(db.Products, "ProductID", "Name", quoteProduct.ProductID);
            ViewBag.QuoteID = new SelectList(db.Quotes, "QuoteID", "UserID", quoteProduct.QuoteID);
            return View(quoteProduct);
        }

        // GET: QuoteProducts/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QuoteProduct quoteProduct = db.QuoteProducts.Find(id);
            if (quoteProduct == null)
            {
                return HttpNotFound();
            }
            return View(quoteProduct);
        }

        // POST: QuoteProducts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            QuoteProduct quoteProduct = db.QuoteProducts.Find(id);
            db.QuoteProducts.Remove(quoteProduct);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GrupoValorCotizador.Models;
using cotizador.Models;

namespace cotizador.Controllers
{
    public class QuoteStatusController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: QuoteStatus
        public JsonResult Index()
        {
            return Json(db.QuoteStatus.ToList(), JsonRequestBehavior.AllowGet);
        }

        // GET: QuoteStatus/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QuoteStatu quoteStatu = db.QuoteStatus.Find(id);
            if (quoteStatu == null)
            {
                return HttpNotFound();
            }
            return View(quoteStatu);
        }

        // GET: QuoteStatus/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: QuoteStatus/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "QuoteStatuID,TextQuote")] QuoteStatu quoteStatu)
        {
            if (ModelState.IsValid)
            {
                db.QuoteStatus.Add(quoteStatu);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(quoteStatu);
        }

        // GET: QuoteStatus/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QuoteStatu quoteStatu = db.QuoteStatus.Find(id);
            if (quoteStatu == null)
            {
                return HttpNotFound();
            }
            return View(quoteStatu);
        }

        // POST: QuoteStatus/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "QuoteStatuID,TextQuote")] QuoteStatu quoteStatu)
        {
            if (ModelState.IsValid)
            {
                db.Entry(quoteStatu).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(quoteStatu);
        }

        // GET: QuoteStatus/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            QuoteStatu quoteStatu = db.QuoteStatus.Find(id);
            if (quoteStatu == null)
            {
                return HttpNotFound();
            }
            return View(quoteStatu);
        }

        // POST: QuoteStatus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            QuoteStatu quoteStatu = db.QuoteStatus.Find(id);
            db.QuoteStatus.Remove(quoteStatu);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

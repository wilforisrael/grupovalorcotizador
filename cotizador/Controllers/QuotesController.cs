﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GrupoValorCotizador.Models;
using cotizador.Models;

namespace cotizador.Controllers
{
    public class QuotesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Quotes
        public ActionResult Index()
        {
            var quotes = db.Quotes.Include(q => q.QuoteStatu);
            return View(quotes.ToList());
        }

        // GET: Quotes/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Quote quote = db.Quotes.Find(id);
            if (quote == null)
            {
                return HttpNotFound();
            }
            return View(quote);
        }

        // GET: Quotes/Create
        public ActionResult Create()
        {
             ViewBag.QuoteStatuID = new SelectList(db.QuoteStatus, "QuoteStatuID", "TextQuote");
            ViewBag.UserID = new SelectList(db.Users, "Id", "Email");
            return View();

        }

        // GET: Quotes/Create
        public ActionResult CreateQuote()
        {
            /* ViewBag.QuoteStatuID = new SelectList(db.QuoteStatus, "QuoteStatuID", "TextQuote");
            ViewBag.UserID = new SelectList(db.Users, "Id", "Email");*/
            return View();

        }

        // POST: Quotes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "QuoteID,UserID,QuoteStatuID,Date,IVA,Discount,Total,TotalPay,Tax")] Quote quote)
        {
            if (ModelState.IsValid)
            {
                db.Quotes.Add(quote);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.QuoteStatuID = new SelectList(db.QuoteStatus, "QuoteStatuID", "TextQuote", quote.QuoteStatuID);
          //  ViewBag.UserID = new SelectList(db.Users, "Id", "Email", quote.UserID);
            return View(quote);
        }

        // GET: Quotes/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Quote quote = db.Quotes.Find(id);
            if (quote == null)
            {
                return HttpNotFound();
            }
            ViewBag.QuoteStatuID = new SelectList(db.QuoteStatus, "QuoteStatuID", "TextQuote", quote.QuoteStatuID);
            //ViewBag.UserID = new SelectList(db.Users, "Id", "Email", quote.UserID);
            return View(quote);
        }

        // POST: Quotes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "QuoteID,UserID,QuoteStatuID,Date,IVA,Discount,Total,TotalPay,Tax")] Quote quote)
        {
            if (ModelState.IsValid)
            {
                db.Entry(quote).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.QuoteStatuID = new SelectList(db.QuoteStatus, "QuoteStatuID", "TextQuote", quote.QuoteStatuID);
        //    ViewBag.UserID = new SelectList(db.Users, "Id", "Email", quote.UserID);
            return View(quote);
        }

        // GET: Quotes/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Quote quote = db.Quotes.Find(id);
            if (quote == null)
            {
                return HttpNotFound();
            }
            return View(quote);
        }

        // POST: Quotes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Quote quote = db.Quotes.Find(id);
            db.Quotes.Remove(quote);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

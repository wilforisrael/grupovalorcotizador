﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GrupoValorCotizador.Models;
using cotizador.Models;

namespace cotizador.Controllers
{
    public class PackageProductsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: PackageProducts
        public ActionResult Index()
        {
            var packageProduct = (from qu in db.Quotes
                                  join u in db.Usercotizes
                                  on qu.UsercotizeID equals u.UsercotizeID.ToString()
                                  join v in db.Vendors
                                  on qu.VendorID equals v.VendorID.ToString() into yG
                                  from y1 in yG.DefaultIfEmpty()
                                      //join qqq in db.QuoteProducts
                                      //on qu.QuoteID equals qqq.QuoteID
                                      //join p in db.Products
                                      //on qqq.ProductID equals p.ProductID
                                      //join pac in db.Packages1
                                      //on qqq.PackageID equals pac.PackageID
                                  select new ResultadosViewModel
                                  {
                                      FirstName = u.FirstName,
                                      Lastname = u.LastName,
                                      RUC = u.RUC,
                                      Total = qu.TotalPay,
                                      Fecha = qu.Date,
                                      QuoteID = qu.QuoteID,
                                      VendedorName = y1.Vendorname
                                  });


            /* select new PackagesproViewModels
             { Name= pr.Name, ProductID = pr.ProductID, Price = pr.Price, Existence= pr.Existence,
                 Discount= pr.Discount, PackageID = p.PackageID, NamePackage =  p.NamePackage,
                 PpCant = pp.Cant, PpDisc = pp.Discount}).ToList();*/
            if (packageProduct == null)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            return View(packageProduct);
        }

        // GET: PackageProducts/Details/5
        public ActionResult Details2()
        {
            var packageProduct = (from qu in db.Quotes
                                  join u in db.Usercotizes
                                  on qu.UsercotizeID equals u.UsercotizeID.ToString()
                                  join v in db.Vendors
                                  on qu.VendorID equals v.VendorID.ToString()
                                  //join qqq in db.QuoteProducts
                                  //on qu.QuoteID equals qqq.QuoteID
                                  //join p in db.Products
                                  //on qqq.ProductID equals p.ProductID
                                  //join pac in db.Packages1
                                  //on qqq.PackageID equals pac.PackageID
                                  select new ResultadosViewModel
                                  { FirstName = u.FirstName,
                                  Lastname = u.LastName, RUC = u.RUC, Total = qu.TotalPay, Fecha = qu.Date,
                                      QuoteID = qu.QuoteID, VendedorName = v.Vendorname
                                  });
                                  
                                  
                                 /* select new PackagesproViewModels
                                  { Name= pr.Name, ProductID = pr.ProductID, Price = pr.Price, Existence= pr.Existence,
                                      Discount= pr.Discount, PackageID = p.PackageID, NamePackage =  p.NamePackage,
                                      PpCant = pp.Cant, PpDisc = pp.Discount}).ToList();*/
            if (packageProduct == null)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            return View(packageProduct);
        }

        public ActionResult ForArticle(long QuoteID)
        {
            var packageProduct = (from  qqq in db.QuoteProducts
                                  join p in db.Products
                                  on qqq.ProductID equals p.ProductID
                                  join pa in db.Packages1
                                  on qqq.PackageID equals pa.PackageID
                                  where qqq.QuoteID == QuoteID
                                   
                                  select new ForArticle2ViewModel
                                  {
                                     Name = p.Name,
                                     Cant = qqq.Cant,
                                     Discount = qqq.Discount,
                                     Price = p.Price,
                                     NamePackage = pa.NamePackage
                                  }).ToList();


            /* select new PackagesproViewModels
             { Name= pr.Name, ProductID = pr.ProductID, Price = pr.Price, Existence= pr.Existence,
                 Discount= pr.Discount, PackageID = p.PackageID, NamePackage =  p.NamePackage,
                 PpCant = pp.Cant, PpDisc = pp.Discount}).ToList();*/
            if (packageProduct == null)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            return View(packageProduct);
        }

        public JsonResult GetArticles(long? id)
        {
            if (id == null)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            var packageProduct = (from p in db.Packages1
                                  join pp in db.Packages
                                  on p.PackageID equals pp.PackageID
                                  join pr in db.Products
                                  on pp.ProductID equals pr.ProductID
                                  where p.PackageID == id
                                  select new 
                                  {
                                      Name = pr.Name,
                                      ProductID = pr.ProductID,
                                      Price = pr.Price,
                                      Existence = pr.Existence,
                                      Discount = pr.Discount,
                                      PackageID = p.PackageID,
                                      NamePackage = p.NamePackage,
                                      PpCant = pp.Cant,
                                      PpDisc = pp.Discount
                                  }).ToList();
            if (packageProduct == null)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
            return Json(packageProduct, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetVendors()
        {

            return Json(db.Vendors.Take(2).ToList(), JsonRequestBehavior.AllowGet);
        }

        // GET: PackageProducts/Create
        public ActionResult Create()
        {
            ViewBag.PackageID = new SelectList(db.Packages1, "PackageID", "NamePackage");
            ViewBag.ProductID = new SelectList(db.Products, "ProductID", "Name");
            return View();
        }

        // POST: PackageProducts/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PackageProductID,PackageID,ProductID,Cant,Discount,Tax")] PackageProduct packageProduct)
        {
            if (ModelState.IsValid)
            {
                db.Packages.Add(packageProduct);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PackageID = new SelectList(db.Packages1, "PackageID", "NamePackage", packageProduct.PackageID);
            ViewBag.ProductID = new SelectList(db.Products, "ProductID", "Name", packageProduct.ProductID);
            return View(packageProduct);
        }
        enum Pack { Es = 1, Nop = 3 };
        enum Vend {  Nop = 3 };
        enum Days { Sat = 1, Sun, Mon, Tue, Wed, Thu, Fri };

        [HttpPost]
        public JsonResult CreatePostQuote(List<elements> form, List<userData> user)
        {
            Usercotize NewUser = new Usercotize
            {
                FirstName = user[0].FirstName,
                LastName = user[0].Lastname,
                RUC = user[0].RUC
            };
            db.Usercotizes.Add(NewUser);
            db.SaveChanges();

            Int64 Vend = 0;
            if (user[0].VendedorID == 0)
            {
                Vend = 3;
            }
            else
            {
                Vend = user[0].VendedorID;
            }

            Quote NewQuote = new Quote
            {
                UsercotizeID = NewUser.UsercotizeID.ToString(),
                Date = DateTime.Now,
                IVA = 14,
                TotalPay = user[0].Total,
                QuoteStatuID = (long)Days.Sat,
                VendorID = Vend.ToString()
                 
            };
            db.Quotes.Add(NewQuote);
            db.SaveChanges();
            
                    
            foreach (var a in form)
            {
                
                if (a.PackageID == 0) {
                    
        QuoteProduct NEWPro = new QuoteProduct
                    {
                        Cant = a.Cant,
                        ProductID = a.ProductID,
                        Discount = a.Discount,
                        PackageID = (long)Pack.Nop,
                        Tax = 0,
                        QuoteID = NewQuote.QuoteID

                    };
                    db.QuoteProducts.Add(NEWPro);
                }
                else{
                    QuoteProduct NEWPro = new QuoteProduct
                    {
                        Cant = a.Cant,
                        ProductID = a.ProductID,
                        PackageID = a.PackageID,
                        Discount = a.Discount,
                        Tax = 0,
                        QuoteID = NewQuote.QuoteID

                    };
                    db.QuoteProducts.Add(NEWPro);
                }  
               
                
                db.SaveChanges();
            }
            return Json("Ok", JsonRequestBehavior.AllowGet);
        }

        // GET: PackageProducts/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PackageProduct packageProduct = db.Packages.Find(id);
            if (packageProduct == null)
            {
                return HttpNotFound();
            }
            ViewBag.PackageID = new SelectList(db.Packages1, "PackageID", "NamePackage", packageProduct.PackageID);
            ViewBag.ProductID = new SelectList(db.Products, "ProductID", "Name", packageProduct.ProductID);
            return View(packageProduct);
        }

        // POST: PackageProducts/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PackageProductID,PackageID,ProductID,Cant,Discount,Tax")] PackageProduct packageProduct)
        {
            if (ModelState.IsValid)
            {
                db.Entry(packageProduct).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PackageID = new SelectList(db.Packages1, "PackageID", "NamePackage", packageProduct.PackageID);
            ViewBag.ProductID = new SelectList(db.Products, "ProductID", "Name", packageProduct.ProductID);
            return View(packageProduct);
        }

        // GET: PackageProducts/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PackageProduct packageProduct = db.Packages.Find(id);
            if (packageProduct == null)
            {
                return HttpNotFound();
            }
            return View(packageProduct);
        }

        // POST: PackageProducts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            PackageProduct packageProduct = db.Packages.Find(id);
            db.Packages.Remove(packageProduct);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

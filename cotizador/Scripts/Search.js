﻿var tota = 0;
var totalf = 0;
    $("#Products").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/Products/GetProducts",
                type: "POST",
                dataType: "json",
                data: { Prefix: request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        return { label: item.Name + " ("+ item.Existence+" Unidades)", value: item.Name, Id: item.ProductID, Cant: item.Existence };
                    }))
                }
            })
        },
        select: function (event, ui) {
            LoadProduct(ui.item.Id)
            return true;
        },
    });

    var elements = [];
    var a = [];
    function LoadProduct(ProductID) {
        $.ajax({
            url: "/Products/Details",
            type: "GET",
            dataType: "json",
            data: "id="+ProductID,
            success: function (data) {
                var result = $.grep(elements, function (e) { return e.ProductID == data.ProductID; });

                if (elements.length > 0 && result.length > 0 && result[0].PackageID == 0) {
                    var s =(result[0].Cant +1);
                    if (s > data.Existence) {
                        alert("No puedes usar el paguete ya que no hay stock suficiente me mercacncia");
                    } else {
                        var ant = (parseInt($("#p" + data.ProductID + "").val()) + 1);
                        var max = parseInt($("#p" + data.ProductID + "").attr('max'));
                        if (parseInt(ant) <= parseInt(max)) {
                            var inputvalue = parseInt($("#p" + data.ProductID + "").val());
                            $("#p" + data.ProductID + "").val(inputvalue + 1);
                            var change = $("#p" + data.ProductID + "").val();
                            $.each(elements, function () {
                                if (this.ProductID == data.ProductID) {
                                    this.Total = this.Total * change;
                                    this.Cant = this.Cant + 1;
                                    $('#MyTable tbody tr#' + data.ProductID + ' td:nth-child(5)').text(this.Total);
                                    
                                }
                            });
                        }
                    }
                   
                } else {
                    var result = $.grep(elements, function (e) { return e.ProductID == data.ProductID; });
                    if (result.length > 1) {
                        var a = result;
                        var band = 0;
                        var sumcant = 0;
                        $.each(a, function (i) {
                            sumcant += a[i].Cant;
                            if (result[i].PackageID != 0 && result[i].ProductID == data.ProductID) {
                                band = 1;
                            }
                        });
                    } else if (result.length > 0) {
                        sumcant = result[0].Cant;
                    }
                    if (elements.length > 0 && result.length > 0 && band == 1) {
                        var s = (sumcant + 1);
                        if (s > data.Existence) {
                            alert("No puedes usar el paguete ya que no hay stock suficiente de mercancia");
                            DeleteRow(data.ProductID, data.PackageID);
                        } else if (s <= data.Existence) {
                            elements.push({ 'ProductID': data.ProductID, 'Cant': 1, 'Name': data.Name, 'PackageID': 0, 'Price': data.Price, 'Existence': data.Existence, 'Discount': data.Discount, 'Total': (data.Price - (data.Price * (data.Discount / 100))) });
                            if (elements.length > arraysize) {
                                var lastEl = elements[elements.length - 1];
                                $('#MyTable tbody').append('<tr id="' + lastEl.ProductID + '"><td><input type="number" onchange="ChangeRow('
                                + lastEl.ProductID + ',this.value)" size="2" id="p' + lastEl.ProductID +
                                '" min="1" value="1" max="30" /></td><td>' + lastEl.Name + '</td><td>' + lastEl.Price + '</td><td>' + lastEl.Discount
                                + '</td><td>' + lastEl.Total + '</td><td><button type="button" id="' + lastEl.ProductID + '" onclick="DeleteRow(' + lastEl.ProductID + ',' + lastEl.PackageID +
                                    ');" class="btn btn-danger">Eliminar</button></td></tr>');
                                $("#p" + data.ProductID + "").attr({ "max": data.Existence });
                                
                            }
                            arraysize = elements.length;
                        }
                       
                    } else {
                        var s = (sumcant + 1);
                        if (s > data.Existence) {
                            alert("No puedes usar el paguete ya que no hay stock suficiente de mercancia");
                            DeleteRow(data.ProductID, data.PackageID);
                        } else {
                            arraysize = elements.length;
                            elements.push({ 'ProductID': data.ProductID, 'Cant': 1, 'Name': data.Name, 'PackageID': 0, 'Price': data.Price, 'Existence': data.Existence, 'Discount': data.Discount, 'Total': (data.Price - (data.Price * (data.Discount / 100))) });
                            if (elements.length > arraysize) {
                                var lastEl = elements[elements.length - 1];
                                $('#MyTable tbody').append('<tr id="' + lastEl.ProductID + '"><td><input type="number" onchange="ChangeRow('
                                + lastEl.ProductID + ',this.value)" size="2" id="p' + lastEl.ProductID +
                                '" min="1" value="1" max="30" /></td><td>' + lastEl.Name + '</td><td>' + lastEl.Price + '</td><td>' + lastEl.Discount
                                + '</td><td>' + lastEl.Total + '</td><td><button type="button" id="' + lastEl.ProductID + '" onclick="DeleteRow(' + lastEl.ProductID + ',' + lastEl.PackageID +
                                    ');" class="btn btn-danger">Eliminar</button></td></tr>');
                                $("#p" + data.ProductID + "").attr({ "max": data.Existence });
                               
                            }
                        }
                        
                    }
                    
                    
                }   
            }
        })
    }

    
    function ChangeRow(ProductID,value) {
        var originalprice=0;
            $.ajax({
                url: "/Products/Details",
                type: "GET",
                dataType: "json",
                data: "id="+ProductID,
                success: function (data) {
                    originalprice = data.Price;
                    $.each(elements, function () {
                        if (this.ProductID == ProductID) {
                            this.Total = (originalprice - (originalprice * (data.Discount / 100))) * value;
                            $('#MyTable tbody tr#' + ProductID + ' td:nth-child(5)').text(this.Total);
                            
                        }
                    });
                }
            })
        
    }

    $('#btn').click(function () {
        var id = $(this).attr('id');
        alert(id);
    });

    $('#SaveButton').click(function () {
        tota = 0;
        $.each(elements, function (i) {
            var casi = parseInt(elements[i].Total);
            tota += casi;
        });
        var tota2 = tota + (tota / 14);
        $('#tota').text(tota);
        $('#tota2').text(Math.round(tota2));
    });

    $('#Confirm').click(function () {
        if (elements.length > 0) {
            tota = 0;
            $.each(elements, function (i) {
                var casi = parseInt(elements[i].Total);
                tota += casi;
            });
           
            var tota2 = tota + (tota / 14);
            $('#tota').text(tota);
            $('#tota2').text(Math.round(tota2));
            var conceptName = $('#optionse').val();
            var user = [{
                'FirstName': $('#FirstName').val(),
                'LastName': $('#LastName').val(),
                'RUC': $('#Ruc').val(),
                'VendedorID': conceptName,
                'VendedorName': null,
                'Total': tota2,
                'QuoteStatusID':null
                }]

            $.ajax({
                url: "/PackageProducts/CreatePostQuote",
                type: "POST",
                dataType: "json",
                data: { form: elements, user },
                success: function (data) {
                    window.location = "http://localhost:26064/PackageProducts";
                }
            })
        }
        
    });
 
    function DeleteRow(ProductID,PackageID) {
        var i = elements.length;
        
        if (PackageID != 0) {
            while (i--) {
                if (elements[i].PackageID == PackageID) {
                    $('#' + elements[i].ProductID + '').remove();
                    elements.splice(i, 1);
                    
                }
            }
            $.each(elements, function (i) {
                total = elements[i].Total;
                $('#total').text(total);
            });
        }
        else {
            $.each(elements, function (i) {
                if (elements[i].ProductID === ProductID) {
                    elements.splice(i, 1);
                    return false;
                } 
            });
            $('#' + ProductID + '').remove();
            $.each(elements, function (i) {
                total = elements[i].Total;
                $('#total').text(total);
            });
        }
    }

    $("#Products").click(function () {
        if ($("#Products").val() != null || $("#Products").val() != "")
            $("#Products").val("");
    });

    var Package =[];
        if (Package.length == 0) {
            $.ajax({
                url: "/Packages/Index2",
                type: "GET",
                dataType: "json",
                success: function (data) {
                    var options = $("#options");
                    $.each(data, function (key, value) {
                        Package = [{ 'PackageID': value.PackageID, 'NamePackage': value.NamePackage }]
                        console.log(value.NamePackage);
                        $("#dropdown select").append('<option value=' + value.PackageID + '>' + value.NamePackage + '</option>');
                    });
                }
            })
        }


        var type = [];
        if (Package.length == 0) {
            $.ajax({
                url: "/PackageProducts/GetVendors",
                type: "GET",
                dataType: "json",
                success: function (data) {
                    var options = $("#optionse");
                    $.each(data, function (key, value) {
                        Package = [{ 'VendorID': value.VendorID, 'TextQuote': value.Vendorname }]
                        console.log(value.NamePackage);
                        $("#dropdown2 select").append('<option value=' + value.VendorID + '>' + value.Vendorname + '</option>');
                    });
                }
            })
        }

        $("#options").change(function () {
            $.ajax({
                url: "/PackageProducts/GetArticles",
                type: "GET",
                dataType: "json",
                data: { id: $('#options').val() },
                success: function (data) {
                    $.each(data, function (key, value) {
                        if (elements.length > 0) {
                            var result = $.grep(elements, function (e) { return e.ProductID == value.ProductID; });
                            if ( data.PackageID != result.PackageID && elements.length > 0 && result.length > 0) {
                                var ant = (parseInt($("#p" + data.ProductID + "").val()) + 1);
                                var max = parseInt($("#p" + data.ProductID + "").attr('max'));
                                if (parseInt(ant) <= parseInt(max)) {
                                    var inputvalue = parseInt($("#p" + data.ProductID + "").val());
                                    $("#p" + data.ProductID + "").val(inputvalue + 1);
                                    var change = $("#p" + data.ProductID + "").val();
                                    $.each(elements, function () {
                                        if (this.ProductID == data.ProductID) {
                                            this.Total = this.Total * change;
                                            $('#MyTable tbody tr#' + data.ProductID + ' td:nth-child(5)').text(this.Total);
                                            
                                        }
                                    });
                                   
                                }
                            } else {
                                if (result.length > 0) {
                                    var s = (result[0].Cant + 1);
                                    if (s > value.Existence) {
                                        alert("No puedes usar el paguete ya que no hay stock suficiente me mercacncia");
                                        DeleteRow(0, value.PackageID)
                                    }
                                } else {
                                    arraysize = elements.length;
                                    elements.push({ 'ProductID': value.ProductID, 'Cant': value.PpCant, 'PackageID': value.PackageID, 'Name': value.Name + ' P.' + value.NamePackage, 'Price': value.Price, 'Existence': value.Existence, 'Discount': value.PpDisc, 'Total': (value.Price - (value.Price * (value.PpDisc / 100))) * value.PpCant });
                                    if (elements.length > arraysize) {
                                        var lastEl = elements[elements.length - 1];
                                        $('#MyTable tbody').append('<tr id="' + lastEl.ProductID + '"><td><input type="number" readonly value="' + value.PpCant + '" size="2" id="p' + lastEl.ProductID +
                                 '" min="1" value="1" max="30" /></td><td>' + lastEl.Name + '</td><td>' + lastEl.Price + '</td><td>' + lastEl.Discount
                                 + '</td><td>' + lastEl.Total + '</td><td><button type="button" id="' + lastEl.ProductID + '" onclick="DeleteRow(' + lastEl.ProductID + ',' + lastEl.PackageID +
                                ');" class="btn btn-danger">Eliminar</button></td></tr>');
                                        $("#p" + value.ProductID + "").attr({ "max": value.Existence });
                                       
                                    }
                                }     
                            }
                        } else {
                            elements.push({ 'ProductID': value.ProductID, 'Cant': value.PpCant, 'PackageID': value.PackageID, 'Cant': value.PpCant, 'Name': value.Name + ' P.' + value.NamePackage, 'Price': value.Price, 'Existence': value.Existence, 'Discount': value.PpDisc, 'Total': (value.Price - (value.Price * (value.PpDisc / 100))) * value.PpCant });
                            var lastEl = elements[elements.length - 1];
                            $('#MyTable tbody').append('<tr id="' + lastEl.ProductID + '"><td><input type="number" readonly value="'+value.PpCant+'"  size="2" id="p' + lastEl.ProductID +
                            '" min="1" value="1" max="30" /></td><td>' + lastEl.Name  +'</td><td>' + lastEl.Price + '</td><td>' + lastEl.Discount
                            + '</td><td>' + lastEl.Total + '</td><td><button type="button" id="' + lastEl.ProductID + '" onclick="DeleteRow(' + lastEl.ProductID + ',' + lastEl.PackageID +
                            ');" class="btn btn-danger">Eliminar</button></td></tr>');
                            $("#p" + value.ProductID + "").attr({ "max": value.Existence });
                            
                        }
                        
                    });
                }
            })
        });

   
 
//})
           

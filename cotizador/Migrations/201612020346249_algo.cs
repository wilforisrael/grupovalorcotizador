namespace cotizador.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class algo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ForArticleViewModels",
                c => new
                    {
                        QuoteID = c.Long(nullable: false, identity: true),
                        ProductID = c.Long(nullable: false),
                        Name = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Cant = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        NamePackage = c.String(),
                    })
                .PrimaryKey(t => t.QuoteID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ForArticleViewModels");
        }
    }
}

namespace cotizador.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class packagetables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PackageProducts",
                c => new
                    {
                        PackageProductID = c.Long(nullable: false, identity: true),
                        PackageID = c.Long(nullable: false),
                        ProductID = c.Long(nullable: false),
                        Cant = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Tax = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.PackageProductID)
                .ForeignKey("dbo.Packages", t => t.PackageID, cascadeDelete: true)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .Index(t => t.PackageID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.Packages",
                c => new
                    {
                        PackageID = c.Long(nullable: false, identity: true),
                        NamePackage = c.String(),
                    })
                .PrimaryKey(t => t.PackageID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PackageProducts", "ProductID", "dbo.Products");
            DropForeignKey("dbo.PackageProducts", "PackageID", "dbo.Packages");
            DropIndex("dbo.PackageProducts", new[] { "ProductID" });
            DropIndex("dbo.PackageProducts", new[] { "PackageID" });
            DropTable("dbo.Packages");
            DropTable("dbo.PackageProducts");
        }
    }
}

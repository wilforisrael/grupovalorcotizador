namespace cotizador.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class algo2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Vendors", "Vendorname", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Vendors", "Vendorname", c => c.Long(nullable: false));
        }
    }
}

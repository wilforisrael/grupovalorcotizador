namespace cotizador.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixlast : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Quotes", "UserID", "dbo.AspNetUsers");
            DropIndex("dbo.Quotes", new[] { "UserID" });
            CreateTable(
                "dbo.Usercotizes",
                c => new
                    {
                        UsercotizeID = c.Long(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        RUC = c.String(),
                    })
                .PrimaryKey(t => t.UsercotizeID);
            
            CreateTable(
                "dbo.Vendors",
                c => new
                    {
                        VendorID = c.Long(nullable: false, identity: true),
                        Vendorname = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.VendorID);
            
            AddColumn("dbo.Quotes", "UsercotizeID", c => c.String());
            AddColumn("dbo.Quotes", "VendorID", c => c.String());
            AddColumn("dbo.Quotes", "Usercotize_UsercotizeID", c => c.Long());
            AddColumn("dbo.Quotes", "Vendor_VendorID", c => c.Long());
            CreateIndex("dbo.Quotes", "Usercotize_UsercotizeID");
            CreateIndex("dbo.Quotes", "Vendor_VendorID");
            AddForeignKey("dbo.Quotes", "Usercotize_UsercotizeID", "dbo.Usercotizes", "UsercotizeID");
            AddForeignKey("dbo.Quotes", "Vendor_VendorID", "dbo.Vendors", "VendorID");
            DropColumn("dbo.Quotes", "UserID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Quotes", "UserID", c => c.String(maxLength: 128));
            DropForeignKey("dbo.Quotes", "Vendor_VendorID", "dbo.Vendors");
            DropForeignKey("dbo.Quotes", "Usercotize_UsercotizeID", "dbo.Usercotizes");
            DropIndex("dbo.Quotes", new[] { "Vendor_VendorID" });
            DropIndex("dbo.Quotes", new[] { "Usercotize_UsercotizeID" });
            DropColumn("dbo.Quotes", "Vendor_VendorID");
            DropColumn("dbo.Quotes", "Usercotize_UsercotizeID");
            DropColumn("dbo.Quotes", "VendorID");
            DropColumn("dbo.Quotes", "UsercotizeID");
            DropTable("dbo.Vendors");
            DropTable("dbo.Usercotizes");
            CreateIndex("dbo.Quotes", "UserID");
            AddForeignKey("dbo.Quotes", "UserID", "dbo.AspNetUsers", "Id");
        }
    }
}

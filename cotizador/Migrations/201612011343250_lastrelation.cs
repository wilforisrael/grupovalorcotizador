namespace cotizador.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lastrelation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.QuoteProducts", "PackageID", c => c.Long(nullable: false));
            CreateIndex("dbo.QuoteProducts", "PackageID");
            AddForeignKey("dbo.QuoteProducts", "PackageID", "dbo.Packages", "PackageID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.QuoteProducts", "PackageID", "dbo.Packages");
            DropIndex("dbo.QuoteProducts", new[] { "PackageID" });
            DropColumn("dbo.QuoteProducts", "PackageID");
        }
    }
}

namespace cotizador.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class second : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductID = c.Long(nullable: false, identity: true),
                        Name = c.String(),
                        Description = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Existence = c.Long(nullable: false),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Tax = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ProductID);
            
            CreateTable(
                "dbo.QuoteProducts",
                c => new
                    {
                        QuoteProductID = c.Long(nullable: false, identity: true),
                        QuoteID = c.Long(nullable: false),
                        ProductID = c.Long(nullable: false),
                        Cant = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Tax = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.QuoteProductID)
                .ForeignKey("dbo.Products", t => t.ProductID, cascadeDelete: true)
                .ForeignKey("dbo.Quotes", t => t.QuoteID, cascadeDelete: true)
                .Index(t => t.QuoteID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.Quotes",
                c => new
                    {
                        QuoteID = c.Long(nullable: false, identity: true),
                        UserID = c.String(maxLength: 128),
                        QuoteStatuID = c.Long(nullable: false),
                        Date = c.DateTime(nullable: false),
                        IVA = c.Int(nullable: false),
                        Discount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Total = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalPay = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Tax = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.QuoteID)
                .ForeignKey("dbo.QuoteStatus", t => t.QuoteStatuID, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserID)
                .Index(t => t.UserID)
                .Index(t => t.QuoteStatuID);
            
            CreateTable(
                "dbo.QuoteStatus",
                c => new
                    {
                        QuoteStatuID = c.Long(nullable: false, identity: true),
                        TextQuote = c.String(),
                    })
                .PrimaryKey(t => t.QuoteStatuID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Quotes", "UserID", "dbo.AspNetUsers");
            DropForeignKey("dbo.Quotes", "QuoteStatuID", "dbo.QuoteStatus");
            DropForeignKey("dbo.QuoteProducts", "QuoteID", "dbo.Quotes");
            DropForeignKey("dbo.QuoteProducts", "ProductID", "dbo.Products");
            DropIndex("dbo.Quotes", new[] { "QuoteStatuID" });
            DropIndex("dbo.Quotes", new[] { "UserID" });
            DropIndex("dbo.QuoteProducts", new[] { "ProductID" });
            DropIndex("dbo.QuoteProducts", new[] { "QuoteID" });
            DropTable("dbo.QuoteStatus");
            DropTable("dbo.Quotes");
            DropTable("dbo.QuoteProducts");
            DropTable("dbo.Products");
        }
    }
}
